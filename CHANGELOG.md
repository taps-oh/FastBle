## v1.1.0

- 名称由@ohos/fastble-ets修改为@ohos/fastble
- 旧的包@ohos/fastble-ets已不维护，请使用新包@ohos/fastble

## v1.0.1

- api8升级到api9 stage模型
- 解决一些bug，如写操作的crash问题，解析权限处理流
- 修改描述符操作，为搜索结果添加设备名称

## v1.0.0

- 已实现功能
  1.初始化
  2.扫描
  3.连接
  4.取消扫描
  5.打开蓝牙
  6.关闭蓝牙
  7.读Characteristic
  8.写Characteristic