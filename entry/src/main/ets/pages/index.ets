/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import router from '@ohos.router'
import bluetooth from '@ohos.bluetoothManager';
import geolocation from '@ohos.geoLocationManager';
import prompt from '@ohos.promptAction';

import {BleManager} from '@ohos/fastble'
import {BleException} from '@ohos/fastble'
import {BleDevice} from '@ohos/fastble'
import {BleScanCallback} from '@ohos/fastble';
import {BleGattCallback} from '@ohos/fastble';
import {BleScanRuleConfig} from '@ohos/fastble'
import {ArrayHelper, PermissionHelper} from './Utils'
import ProgressDialog from './ProgressDialog'

@Entry
@Preview
@Component
struct Page {
    @State private edit_text_name_hint: string = '';
    @State private edit_text_mac_hint: string = '';
    @State private edit_text_uuid_hint: string = '';

    @State edit_text_uuid: string = '';
    @State edit_text_name: string = '';
    @State edit_text_mac: string = '';
    @State switch_auto_connect: boolean = false;

    @State setting_visibility: Visibility = Visibility.None;
    @State search_setting_text: Resource = $r('app.string.expand_search_settings');
    @State btn_scan_text: Resource = $r('app.string.start_scan');
    @State is_loading: boolean = false;
    @State loading_rotate: number = 0;

    @State private toast_open_bluetooth: string = '';
    @State private toast_connect_fail: string = '';
    @State private toast_disconnected: string = '';
    @State private toast_active_disconnected: string = '';

    @State bleDeviceList: BleDevice[] = [];
    @State connectedDevices: string[] = [];

    private loadStrings() {
        let manager = globalThis.context.resourceManager;

        manager.getString($r('app.string.setting_name').id).then(text=>{
            this.edit_text_name_hint = text;
        })
        manager.getString($r('app.string.setting_mac').id).then(text=>{
            this.edit_text_mac_hint = text;
        })
        manager.getString($r('app.string.setting_uuid').id).then(text=>{
            this.edit_text_uuid_hint = text;
        })
        manager.getString($r('app.string.please_open_blue').id).then(text=>{
            this.toast_open_bluetooth = text;
        })
        manager.getString($r('app.string.connect_fail').id).then(text=>{
            this.toast_connect_fail = text;
        })
        manager.getString($r('app.string.disconnected').id).then(text=>{
        this.toast_disconnected = text;
        })
        manager.getString($r('app.string.active_disconnected').id).then(text=>{
            this.toast_active_disconnected = text;
        })
    }

    build() {
        Column() {
            Column() {
                Text($r('app.string.scan_setting')).fontSize(14).fontColor($r('app.color.colorPrimary')).margin({bottom:10})
                TextInput({ placeholder: this.edit_text_name_hint, text: this.edit_text_name })
                    .type(InputType.Normal)
                    .placeholderColor(Color.Gray)
                    .placeholderFont({ size: 14})
                    .margin({bottom:10})
                    .onChange((value: string) => {
                      this.edit_text_name = value;
                    })
                TextInput({ placeholder: this.edit_text_mac_hint, text: this.edit_text_mac })
                    .type(InputType.Normal)
                    .placeholderColor(Color.Gray)
                    .placeholderFont({ size: 14})
                    .margin({bottom:10})
                    .onChange((value: string) => {
                      this.edit_text_mac = value;
                    })
                TextInput({ placeholder: this.edit_text_uuid_hint, text: this.edit_text_uuid })
                    .type(InputType.Normal)
                    .placeholderColor(Color.Gray)
                    .placeholderFont({ size: 14})
                    .margin({bottom:10})
                    .onChange((value: string) => {
                      this.edit_text_uuid = value;
                    })
                Text('AutoConnect ?').fontSize(14)
            }.width('100%').alignItems(HorizontalAlign.Start).padding(20).visibility(this.setting_visibility)
            Button(this.search_setting_text)
                .onClick(()=>this.onSearchSettingClick())
                .fontSize(18)
                .height(80)
                .margin({bottom:10})
            Stack() {
                Button(this.btn_scan_text)
                    .onClick(()=>this.onSearchClick())
                    .fontSize(18)
                    .height(80)
                    .fontWeight(FontWeight.Bold)
                LoadingProgress().width(40).height('100%').align(Alignment.Center)
                    .color($r('app.color.colorPrimary'))
                    .markAnchor({ x: 40+10, y: 0 })
                    .position({ x: '100%', y: 0 })
                    .visibility(this.is_loading ? Visibility.Visible : Visibility.None)
            }.width('100%')
            List(){
                ForEach(this.bleDeviceList, (device: BleDevice) => {
                    ListItem() {
                        Row() {
                            if (this.isConnected(device)) {
                                Image($r('app.media.ic_blue_connected')).objectFit(ImageFit.None).width(30).height(30)
                                Column(){
                                    Text(device.getName()).margin({bottom:2}).fontSize(14).fontColor($r('app.color.colorPrimary'))
                                    Text(device.getMac()).margin({top:2}).fontSize(12).fontColor($r('app.color.colorPrimary'))
                                }.layoutWeight(1).margin({left:10}).alignItems(HorizontalAlign.Start)
                                Row() {
                                    Text($r('app.string.connected')).fontColor($r('app.color.colorPrimary')).fontSize(14)
                                    Button($r('app.string.disconnect')).width(100).height(80).margin({left: 5}).fontSize(12).onClick(()=>this.onDisconnectClick(device))
                                    Button($r('app.string.enter')).width(100).height(80).margin({left: 5}).fontSize(12).onClick(()=>this.onDetailClick(device))
                                }
                            } else {
                                Image($r('app.media.ic_blue_remote')).objectFit(ImageFit.None).width(30).height(30)
                                Column(){
                                    Text(device.getName()).margin({bottom:2}).fontSize(14)
                                    Text(device.getMac()).margin({top:2}).fontSize(12)
                                }.layoutWeight(1).margin({left:10}).alignItems(HorizontalAlign.Start)
                                Row() {
                                    Text('' + device.getRssi()).fontSize(14)
                                    Image($r('app.media.ic_rssi')).objectFit(ImageFit.ScaleDown).width(18).height(18).margin({left: 5});
                                    Button($r('app.string.connect')).width(100).height(80).margin({left: 5}).fontSize(12).onClick(()=>this.onConnectClick(device))
                                }
                            }
                        }.width('100%')
                    }
                }, (item: number) => item.toString())
            }.width('100%').layoutWeight(1).margin({top:10}).padding({left:5, right:5})
            .divider({ strokeWidth: 0.5, color: '#aaa' })
        }
        .width('100%')
        .height('100%')
    }

    private isConnected(device: BleDevice): boolean {
        return ArrayHelper.contains(this.connectedDevices, device.getMac());
    }

    private onSearchSettingClick(): void {
        if(this.setting_visibility == Visibility.Visible) {
            this.setting_visibility = Visibility.None;
            this.search_setting_text = $r('app.string.expand_search_settings')
        }else {
            this.setting_visibility = Visibility.Visible;
            this.search_setting_text = $r('app.string.retrieve_search_settings')
        }
    }

    private onSearchClick(): void {
        if (this.btn_scan_text.id == $r('app.string.start_scan').id) {
            if(BleManager.MOCK_DEVICE) {
                this.mockCheckPermissions();
            } else {
                this.checkPermissions();
            }
        } else if (this.btn_scan_text.id == $r('app.string.stop_scan').id) {
            BleManager.getInstance().cancelScan();
        }
    }

    private onConnectClick(device: BleDevice) {
        if (!BleManager.getInstance().isConnected(device)) {
            BleManager.getInstance().cancelScan();
            this.connect(device);
        }
    }

    private onDisconnectClick(device: BleDevice) {
        if (BleManager.MOCK_DEVICE || BleManager.getInstance().isConnected(device)) {
            BleManager.getInstance().disconnect(device);
        }
    }

    private onDetailClick(device: BleDevice) {
        console.info("onDetailClick deviceName:"+device.getName());
        router.push({
            url: 'pages/ServiceListPage',
            params: {device: device}
         });
    }

    private mockCheckPermissions(): void {
        this.doScan();
    }

    private checkPermissions(): void {
        let result = bluetooth.getState();
        console.info("checkPermissions bluetooth.state: "+ result);
        if(result == bluetooth.BluetoothState.STATE_OFF) {
            prompt.showToast({message: this.toast_open_bluetooth, duration: 300,})
            return;
        }

        let permissions = [
            "ohos.permission.USE_BLUETOOTH",
            "ohos.permission.DISCOVER_BLUETOOTH",
            "ohos.permission.MANAGE_BLUETOOTH",
            "ohos.permission.LOCATION"
        ];
        PermissionHelper.requestPermissions(permissions, results => {
            for(let i=0; i<permissions.length; i++) {
                if(results[i] != 0) {
                    let msg = "permission " + permissions[i] + " not granted!";
                    prompt.showToast({message: msg, duration: 2000})
                    return;
                }
            }
            this.onPermissionGranted();
        });
    }

    private checkGPS(callback: (result: boolean)=>void): void {
        console.info("calling checkGPS")
        // geolocation.isLocationEnabled((err, enabled) => {
        //     console.info("isLocationEnabled err:"+err+", enabled:"+enabled)
        //     callback(!err ? enabled : false);
        // });
        if(geolocation.isLocationEnabled()){
            callback(true);
        }else {
            callback(false);
        }
    }

    private onPermissionGranted(): void {
        console.info("calling onPermissionGranted")
        this.checkGPS(result => {
            console.info("checkGPS result:"+result)
            if(result) {
                this.doScan();
            }else{
                AlertDialog.show({
                    title: $r('app.string.notifyTitle'),
                    message: $r('app.string.gpsNotifyMsg'),
                    confirm: {
                        value: $r('app.string.ok'),
                        action: null
                    }
                })
            }
        });
    }

    private doScan() {
        this.setScanRule();
        this.startScan();
    }

    private setScanRule(): void {
        let uuids: string[];
        let str_uuid: string = this.edit_text_uuid;
        if (!str_uuid) {
            uuids = null;
        } else {
            uuids = str_uuid.split(",");
        }
        let serviceUuids: string[] = null;
        if (uuids != null && uuids.length > 0) {
            serviceUuids = new Array[uuids.length];
            for (let i = 0; i < uuids.length; i++) {
                let name: string = uuids[i];
                let components: string[] = name.split("-");
                if (components.length != 5) {
                    serviceUuids[i] = null;
                } else {
                    serviceUuids[i] = uuids[i];
                }
            }
        }

        let names: string[];
        let str_name: string = this.edit_text_name;
        if (!str_name) {
            names = null;
        } else {
            names = str_name.split(",");
        }

        let mac: string = this.edit_text_mac;

        let isAutoConnect: boolean = this.switch_auto_connect;

        let scanRuleConfig: BleScanRuleConfig = new BleScanRuleConfig.Builder()
                .setServiceUuids(serviceUuids)      // 只扫描指定的服务的设备，可选
                .setDeviceName(true, names)         // 只扫描指定广播名的设备，可选
                .setDeviceMac(mac)                  // 只扫描指定mac的设备，可选
                .setAutoConnect(isAutoConnect)      // 连接时的autoConnect参数，可选，默认false
                .setScanTimeOut(10000)              // 扫描超时时间，可选，默认10秒
                .build();
        BleManager.getInstance().initScanRule(scanRuleConfig);
    }


    public clearConnectedDevice() {
        for (let i = 0; i < this.bleDeviceList.length; i++) {
            let device: BleDevice = this.bleDeviceList[i];
            if (BleManager.getInstance().isConnected(device)) {
                ArrayHelper.removeIndex(this.bleDeviceList, i);
            }
        }
    }

    public clearScanDevice() {
        for (let i = 0; i < this.bleDeviceList.length; i++) {
            let device: BleDevice = this.bleDeviceList[i];
            if (!BleManager.getInstance().isConnected(device)) {
                ArrayHelper.removeIndex(this.bleDeviceList, i);
            }
        }
    }

    public clear() {
        this.bleDeviceList = [];
    }

    private startScan(): void {
        let _this = this;
        console.info("startScan");
        BleManager.getInstance().scan(new class extends BleScanCallback {
            onScanStarted(success: boolean): void {
                console.info("onScanStarted success:"+success);
                _this.clearScanDevice();
                _this.is_loading = true;
                _this.loading_rotate = 360;
                _this.btn_scan_text = $r('app.string.stop_scan');
                _this.connectedDevices = BleManager.getInstance().getAllConnectedDevice();
            }

            onLeScan(bleDevice: BleDevice): void {
                console.info("onLeScan");
            }

            onScanning(bleDevice: BleDevice): void {
                console.info("onScanning");
                ArrayHelper.add(_this.bleDeviceList, bleDevice);
            }

            onScanFinished(scanResultList: Array<BleDevice>): void {
                console.info("onScanFinished");
                _this.is_loading = false;
                _this.loading_rotate = 0;
                _this.btn_scan_text = $r('app.string.start_scan');
                _this.connectedDevices = BleManager.getInstance().getAllConnectedDevice();
            }
        });
    }

    private progressDialogCtrl: CustomDialogController = new CustomDialogController({
        builder: ProgressDialog()
    });

    private connect(bleDevice: BleDevice): void {
        let _this = this;
        BleManager.getInstance().connect(bleDevice, new class extends BleGattCallback {
            public onStartConnect(): void {
                _this.progressDialogCtrl.open();
            }

            public onConnectFail(bleDevice: BleDevice, exception: BleException): void {
                _this.progressDialogCtrl.close();
                prompt.showToast({message: _this.toast_connect_fail, duration: 300,})
            }

            public onConnectSuccess(bleDevice: BleDevice, gatt: bluetooth.GattClientDevice, status: number): void {
                _this.progressDialogCtrl.close();
                ArrayHelper.add(_this.connectedDevices, bleDevice.getMac());
            }

            public onDisConnected(isActiveDisConnected: boolean, device: BleDevice, gatt: bluetooth.GattClientDevice, status: number): void {
                _this.progressDialogCtrl.close();
                _this.connectedDevices = BleManager.getInstance().getAllConnectedDevice();

                if (isActiveDisConnected) {
                    prompt.showToast({message: _this.toast_active_disconnected, duration: 300,})
                } else {
                    prompt.showToast({message: _this.toast_disconnected, duration: 300,})
                }
            }
        });
    }

    aboutToAppear() {
        BleManager.getInstance().init();
        BleManager.getInstance()
                .enableLog(true)
                .setReConnectCount(1, 5000)
                .setConnectOverTime(20000)
                .setOperateTimeout(5000);
        this.loadStrings();
    }

    aboutToDisappear() {
        BleManager.getInstance().disconnectAllDevice();
        BleManager.getInstance().destroy();
    }
}
